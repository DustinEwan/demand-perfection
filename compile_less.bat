@echo off
cd less
for /f "tokens=1 delims=." %%a in ('dir /b *.less') do call lessc -x %%a.less ../styles/%%a.css
cd ..