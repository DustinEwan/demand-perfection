var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost');

exports.connect = function (callback) {
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', callback);
}

var getDpSchema = function () {
    var result = mongoose.Schema.apply(mongoose, arguments);
        
    result.options.toJSON = {
        transform: function (doc, result, options) {
            result.id = result._id;
            delete result._id;
            return result;
        }
    };

    return result;
};

var VectorTypeSchema = getDpSchema({
    name: String,
    minimum: Number,
    maximum: Number,
    directRelationship: Boolean,
    importance: Number
});

var VectorSchema = getDpSchema({
    vector: { type: mongoose.Schema.Types.ObjectId, ref: 'VectorType' },
    value: Number
});

var FactorSchema = getDpSchema({
    name: String,
    description: String,
    vectors: [VectorSchema]
});

var FactorTypeSchema = getDpSchema({
    userId: Number,  // user schema
    name: String,
    factors: [FactorSchema],
    vectorTypes: [VectorTypeSchema]
});

exports.Models = {
    "Factor": mongoose.model('Factor', FactorSchema),
    "Vector": mongoose.model('Vector', VectorSchema),
    "VectorType": mongoose.model('VectorType', VectorTypeSchema),
    "FactorType": mongoose.model('FactorType', FactorTypeSchema),
}


