var port = 3000,
	express = require('express'),
	engines = require('consolidate'),
	passport = require('passport'),
	loginstrategy = require('passport-local').Strategy,
	app = express(),
	database = require('./database');

function genRandomString(length) {
	var str = '';
	while (str.length < length) {
		str = str + Math.random().toString(36).slice(2);
	}

	return str.slice(0, length);
}


// ------ APPLICATION CONFIGURATION ------
app.use(express.cookieParser());
app.use(express.bodyParser());
app.use(express.session({secret: genRandomString(512)}));
app.engine('jade', engines.jade);
app.use('/scripts', express.static(local("/scripts")));
app.use('/styles', express.static(local("/styles")));

function local(dir) {
	return __dirname + dir;
}


// ------ AUTH CONFIGURATION ------
app.use(passport.initialize());
app.use(passport.session());

function authorized(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}

	// auth failure
	res.send(401);
}

var testUser = {
	id: 1,
	username: 'testuser',
	password: 'secret',
	email: 'test@example.com'
};

function findUser(username, done) {
	if (username === testUser.username) {
		// username matched
		return done(null, testUser);
	}
	console.log(testUser.username + ' !== ' + username);
	// failure
	done(null, false);
}

function testPassword(user, password) {
	/*
		When this is all said and done, we should be receiving
		a user whose password is hashed and salted.  We need
		to hash and salt the password that is coming in and
		test the hashes against one another.
	*/
	console.log(user.password + ' === ' + password);
	return user.password === password;
}

passport.use(new loginstrategy(
	function (username, password, done) {
		console.log("Attempting login.");
		findUser(username, function (err, user) {
			if (err) {
				return done(err);
			}

			if (!user || !testPassword(user, password)) {
				return done(null, false, { message: 'Invalid Credentials Provided.' });
			}

			//login successful
			return done(null, user);
		});
	}
));

passport.serializeUser(function(user, done) {
	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	if (id === testUser.id) {
		return done(null, testUser);
	}

	done(null, null);
});

app.get('/auth/login', function (req, res) {
	renderView('login', res);
});

app.post('/auth/login',
	passport.authenticate('local', {
		successRedirect: '/',
		failureRedirect: '/auth/login',
		failureFlash: true
	})
);

app.get('/auth/logout', function (req, res) {
	req.logout();
	res.redirect('/');
});

// ------ ROOT INDEX ------
// for now, simply render the index view
//app.get('/', authorized, function(req, res) {
app.get('/', function(req, res) {
	renderView('index', res);
});


// ------ VIEWS ------
function renderView(view, res) {
	res.render(local('/templates/:view.jade'.replace(':view', view)));
}

app.get('/views/:category', function (req, res) {
    renderView(req.params.category, res)
});

// ------ DATA API ------

//app.all(/^\/api\//, authorized);


app.get('/api/factors', function (req, res) {
    var factorType = database.Models['FactorType'];
    
    factorType.find({}, '-vectorTypes -factors', function (err, fts) {
    	console.log(fts);

        res.send(fts.map(function (item) { return item.toJSON(); }));
    });
});

app.get('/api/factors/:factorName', function (req, res) {
    var factorType = database.Models['FactorType'];

    factorType.findOne({ name: new RegExp(req.params.factorName, 'i'), userId: testUser.id }, function (err, ft) {            
    	console.log(ft);
        ft = ft || {}; // prevent dying on not finding anything.
        res.send({
            factors: ft.factors,
            vectors: ft.vectorTypes
        });
    });
});

app.post('/api/factors/:factorId', function (req, res) {
    var factorId = req.params.factorId,
        factorModel = database.Models['Factor'];
    
    factorModel.findOne({ id: factorId }, function (err, factor) {
        if (err || !factor) {
            //die!
        }
        
        for (var i = 0; i < factor.vectors.length; i++) {
            if (factor.vectors[i].id === vectorId) {
                factor.vectors[i].value = req.body.value;
            }
        }
    });
});

app.post('/api/factors/:factorId/vectors/:vectorId', function (req, res) {
    var factorId = req.params.factorId,
        vectorId = req.params.vectorId,
        factorModel = database.Models['Factor'];
        
    factorModel.findOne({ id: factorId }, function (err, factor) {
        if (err || !factor) {
            //die!
        }

        for (var i = 0; i < factor.vectors.length; i++) {
            if (factor.vectors[i].id === vectorId) {
                factor.vectors[i].value = req.body.value;
            }
        }
    });

});

app.post('/api/factors', function (req, res) {
    var factorType = database.Models['FactorType'],
        factor = database.Models['Factor'];
    console.log('Incoming create: ' + JSON.stringify(req.body));

    factorType.findOne({ name: req.body.factorType, userId: testUser.id }, function (err, ft) {
        if (err || !ft) {
        	console.log(req.body.factorType + " not found, creating.");
            ft = new factorType({ name: req.body.factorType, userId: testUser.id, });
        }

        if (req.body.factor && req.body.factor.name) {
	        console.log('Creating: ' + JSON.stringify(req.body.factor));
	        ft.factors.push(new factor(req.body.factor));
	    }

        ft.save();
    });
});

app.post('/api/factors/:factorName/vectorTypes', function (req, res) {
    var factorType = database.Models['FactorType'],
        vectorType = database.Models['VectorType'];

    factorType.findOne({ name: new RegExp(req.params.factorName, 'i'), userId: testUser.id }, function (err, ft) {
        if (err || !ft) {
        	if (!req.body.factorType) {
        		res.send("Factor Id " + req.params.factorId + " not found.");
        		return;
        	}

            ft = new factorType({ name: req.body.factorType, userId: testUser.id, });
        }

        console.log(ft);
        ft.vectorTypes.push(new vectorType(req.body.vectorType));

        console.log("Creating vector type: " + req.body.vectorType);
        ft.save();
    });
});

app.get('/api/factors/:factorName/vectorTypes', function (req, res) {
    var factorType = database.Models['FactorType'],
        vectorType = database.Models['VectorType'];
    
    factorType.findOne({ name: new RegExp(req.params.factorName, 'i'), userId: testUser.id }, function (err, ft) {
    	console.log(ft);
        res.send({
            vectorTypes: ft === null ? [] : ft.vectorTypes
        });
    });
});

database.connect(function () {
    console.log("Listening on " + port);
    app.listen(port);
});
