require.config({
	baseUrl: "/scripts",

	paths: {
		"jquery": "jquery.min",
		"jquery.ui": "jquery-ui.min",
		"jquery.debounce": "jquery.ba-throttle-debounce.min",
		"signals": "signals.min",
		"crossroads": "crossroads.min",
		"hasher": "hasher.min",
        "knockout": "knockout-min",
        "bootsrap": "bootstrap.min",
        "chartist": "chartist.min"
	},

	shim: {
        'jquery': {
            exports: '$'
        },
        
        'jquery.ui': ['jquery'],
        
        'jquery.debounce': ['jquery'],
        
        'knockout': {
            deps: ['jquery'],
            exports: 'ko'
        },
        
        'hasher': {
            deps: ['signals'],
            exports: 'hasher'
        },
        
        'crossroads': {
            deps: ['signals', 'hasher'],
            exports: 'crossroads'
        },

        'koExternalTemplateEngine': ['knockout'],

        'bootstrap': {
            dps: ['jquery'],
            exports: 'bootstrap'
        },
        'chartist': {
        	exports: 'Chartist'
        }
	}
});

require([
    'knockout',
    'bootstrap',
	'crossroads',
	'hasher',
	'ui/menu',
	'routes',
	'bindinghandlers/bootstrap',
    'jquery.ui',
    'koExternalTemplateEngine',
    'chartist'
], function () {
    infuser.defaults.templateSuffix = '';
});
