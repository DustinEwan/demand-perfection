define(['jquery', 'knockout', 'crossroads', 'jquery.debounce'], function($, ko, xr) {

	// the scrolly out binding handler
	ko.bindingHandlers.rollOut = {
		init: function(element, valueAccessor) {
			var widths = valueAccessor();
			var el = $(element);

			var content = $('#content');

			var duration = 250;
			var delay = 350;

			// roll open
			el.mouseenter(
				$.debounce(
					delay, true,
					function() {
						el.animate({
							width: widths.big
						}, duration);

						//content.animate({
						//	'margin-left': widths.big
						//}, duration);
					}
				)
			);

			// roll closed
			el.mouseleave(
				$.debounce(
					delay, true,
					function() {
						el.animate({
							width: widths.small
						}, duration);

						//content.animate({
						//	'margin-left': widths.small
						//}, duration);
					}
				)
			);
		}
	}


	var buildMenuItem = function(title, icon, targetUrl) {
		return {
			title: title,
			icon: icon,
			targetUrl: targetUrl,
			selected: ko.observable(false)
		};
	}

	var items = [
		buildMenuItem("Customers", "icon-users", '/app/customers'),
		buildMenuItem("Products", "icon-light-bulb", '/app/products'),
		buildMenuItem("Analytics", "icon-statistics", '/app/analytics')
	];

	var menuVm = {
		selectedItem: ko.observable(),
		items:items,
		select: function(item) {
			var old = this.selectedItem();
			if (old) old.selected(false);

			item.selected(true);
			this.selectedItem(item);
			xr.parse(item.targetUrl);
		}
	};

	// I wish there was a better way to do this.
	ko.applyBindings(menuVm, $('#navMenu')[0]);

	return;
});
