define(['knockout'], function(ko) {
	ko.bindingHandlers.odd = {
		update: function (element, valueAccessor) {
			var index = ko.utils.unwrapObservable(valueAccessor());
			ko.bindingHandlers.css.update(element, function() {
				return { 'odd': index % 2 == 1 };
			});
		}
	};
});
