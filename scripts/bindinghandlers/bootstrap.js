// the point of this bootstrapper is to simply load in all of the
// custom binding handlers for we use into knockout.
// when you add a bindinghandler, just add it to this list and it 
// will be loaded into knockout.

define([
	'bindinghandlers/sliderthumb',
	'bindinghandlers/piechart'
], function() {});
