define(['knockout'], function(ko) {
	var self = 
	ko.bindingHandlers.sliderThumb = {
		init: function (element, valuesAccessor) {
			var item = ko.utils.unwrapObservable(valuesAccessor()),
				el$ = $(element),
				hr$ = el$.parent().siblings('hr'),
				sliderWidth = hr$.width()
				sliderOffset = hr$.offset().left;

			el$.addClass('sliderThumb');

			el$.mousedown(function (down) {
				var offset = el$.offset().left - down.pageX - sliderOffset - (el$.height());
				//var offset = el$.offset().left - down.pageX - sliderOffset - (el$.height() / 2) - (el$.width() / 2);

				el$.data('dragging', true);

				$(document).on('mousemove', function (move) {
					var value = move.pageX + offset,
						//vector = ((el$.height() / 2 * 0) + (el$.width() / 2) + value) / sliderWidth,
						vector = ((el$.width() / 2) + value) / sliderWidth,
						color = self.valueToColor(vector, 40, 80),
						bordercolor = self.valueToColor(vector, 80, 40);

					if (0 <= vector && vector <= 1) {
						item.value(vector);
						el$.css({left: value, 'background-color': color, 'border-color': bordercolor});
					}
				});

				$(document).mouseup(function () {
					$(document).off('mousemove');
					el$.data('dragging', false);
				});
				down.preventDefault();

			});

			self.disableSelection(el$);
		},
		
		update: function (element, valuesAccessor) {
			var item = ko.utils.unwrapObservable(valuesAccessor()),
				el$ = $(element),
				hr$ = el$.parent().siblings('hr'),
				sliderWidth = hr$.width()
				valueToPosition = function () {
					return sliderWidth * item.value() - (el$.width() / 2);
				},
				position = valueToPosition();

			ko.bindingHandlers.text.update(element, function () { return item.label; });

			el$.css({
				'left': position,
				'background-color': self.valueToColor(item.value(), 40, 80),
				'border-color': self.valueToColor(item.value(), 80, 40)
			});
		},
		valueToColor: function (value, saturation, lightness) {
			var min = 0,
				max = 160,
				hue = value * max + min;

			return 'hsl(' + hue + ', ' + saturation + '%, ' + lightness + '%)';
		},
		disableSelection: function (element) {
			element.attr('unselectable', 'on')
	               	.css({'-moz-user-select':'-moz-none',
	                     '-moz-user-select':'none',
	                     '-o-user-select':'none',
	                     '-khtml-user-select':'none',
	                     '-webkit-user-select':'none',
	                     '-ms-user-select':'none',
	                     'user-select':'none'})
	               .bind('selectstart', false);
		}
	};
});