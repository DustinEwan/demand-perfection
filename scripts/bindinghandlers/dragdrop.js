
define(['knockout'], function (ko) {
	var move_handler;
	
	$.fn.animateRotate = function(angle, duration, easing, complete) {
		var args = $.speed(duration, easing, complete),
			step = args.step;
		return this.each(function(i, e) {
			e.currentangle = e.now || 0;
			args.step = function(now) {
				e.currentangle = now;
				$.style(e, 'transform', 'rotate(' + now + 'deg)');
				if (step) {
					return step.apply(this, arguments);
				}
			};

			$({deg: e.currentangle}).animate({deg: angle}, args);
		});
	};

	function tilt_direction(item) {
		var last_pos = item.position().left,
			last_dir = 0,
			new_dir = 0,
			move_handler = function (e) {
				new_dir = e.pageX > last_pos ? 1 
					: e.pageX < last_pos ? -1
					: 0;
				
				if (new_dir != last_dir && new_dir != 0) {
					item.stop().animateRotate(new_dir * 10, 50, 'easeOutCirc');
					$('html').unbind('mousemove', move_handler);
				}

				last_dir = new_dir;
				left_pos = e.pageX;
				last_pos = left_pos;
			};
		$("html").bind("mousemove", move_handler);
	}  


	ko.bindingHandlers.container = {
		init: function(element, valueAccessor, allBindingsAccessor, context) {
			var originalParent = context.items,
				$el = $(element);

			if (!$el.hasClass('container')) {
				$el.addClass('container');
			}
		
			$el.sortable({
				update: function(event, ui) {
					// if this is an orphaned item, bail out.
					if (!ui.item || ui.item.parent().length === 0) {
						return;
					}

					var item = ko.dataFor(ui.item[0]),
						newParent = ko.dataFor(ui.item.parent()[0]).items,
						listPosition = ko.utils.arrayIndexOf(ui.item.parent().children(), ui.item[0]);
					if (item) {
						originalParent.remove(item);
						newParent.splice(listPosition, 0, item);
						ui.item.remove(); // this prevents unwired duplicates after adding it to the ko observable
					}
				},
				connectWith: '.container',
				handle: '.drag-handle',
				start: function (event, ui) {
					ui.item.addClass('tilt');
					tilt_direction(ui.item);
				},
				stop: function (event, ui) {
					ui.item.animateRotate(0, 400, 'easeOutElastic', function () {
						ui.item.removeClass("tilt");
					});
				}
			});
		}
	};
});
