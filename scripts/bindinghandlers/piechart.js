define(['knockout', 'chartist'],
function(ko, Chartist) {
	function buildData(valueAccessor) {
		var factors = ko.utils.unwrapObservable(valueAccessor());
		var labels = [], series = [];


		for (var i in factors) {
			labels.push(factors[i].name());
			series.push(factors[i].getVectorNormal());
		}

		// build customers chart
        return {
		  // A labels array that can contain any sort of values
		  labels: labels,
		  // Our series array that contains series objects or in this case series data arrays
		  series: series
		};
	}

	ko.bindingHandlers.pieChart = {
		init: function (element, valueAccessor) {
			var data = buildData(valueAccessor),
				el$ = $(element);

			el$.addClass('ct-chart');

			// Create a new line chart object where as first parameter we pass in a selector
			// that is resolving to our chart container element. The Second parameter
			// is the actual data object.
			el$.data('chart', new Chartist.Pie(element, data));
		},
		update: function (element, valueAccessor) {
			var chart = $(element).data('chart');
			if (chart) {
				chart.data = buildData(valueAccessor);
				chart.update();
			}
		}
	}
});