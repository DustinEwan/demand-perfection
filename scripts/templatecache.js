define([], function () {

	var _cache = {},
		templateCache = function () {
			this.getTemplate = function (route, callback) {
				if (_cache[route]) {
					return callback(_cache[route]);
				}


				$.ajax(route).success(function(data) {
					_cache[route] = data;
					callback(data);
				});
				
			};	
		};
		

	return templateCache;
});
