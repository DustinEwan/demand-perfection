
define(['crossroads',
		'hasher', 
		'knockout', 
		'templatecache', 
		'viewmodels/customers', 
		'viewmodels/products',
		'viewmodels/analytics'], 
	function(xr, hasher, ko, templatecache, customers, products, analytics) {
		// This map correlates the viewmodel with the
		//   data that the api call will be returning.
		var sectionVmMap = {
				'customers': customers,
				'products': products,
				'analytics': analytics
			},
			sectionApiMap = {
				'customers': loadFactorData,
				'products': loadFactorData,
				'analytics': loadApiData
			};

		vmCache = {},
		_cache = new templatecache();

		function switchContent(section, vm) {
			var url = '/views/' + section,
				content = $('#content');
			ko.cleanNode(content[0]);

			_cache.getTemplate(url, function(data) {
					content.html(data);
					ko.applyBindings(vm, content[0]);
				});

			vmCache[section] = vm;
		}

		function loadApiData(section) {
			var url = '/api/' + section;

			var d = null;
			$.ajax(url, { async: false })
				.success(function(data) {
					d = data;
				});	

			return d;
		}

		function loadFactorData(factor) {
			return loadApiData('factors/' + factor);
		}

		function loadData(section) {
			return sectionApiMap[section](section);
		}

		function buildVM(section, data) {
			return sectionVmMap[section](data);	
		}

		xr.addRoute('/app/{section}', function(section) {
			var vm = vmCache[section] || buildVM(section, loadData(section));
			switchContent(section, vm);
		});
});
