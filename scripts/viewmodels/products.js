
define(['knockout', 'viewmodels/factor'],
	function (ko, factor) {
    var buildProduct = function (productData) {
        return new factor(productData);
    };
    
    return function (productData) {
        var cd = productData,
            products = ko.observableArray([]),
            product,
            expectedVectors = ko.observableArray(cd ? cd.vectors : []),
            productData = cd ? cd.factors : [],
            data;
        

        for (var i in productData) {
            data = cd.factors[i];
            data.expectedVectors = expectedVectors();            
            product = buildProduct(data);
            products.push(product);
        }

        return {
            products: products,
            vectors : expectedVectors,
            save: function () {
                for (var i = products().length - 1; i >= 0; i--) {
                    var dto = products()[i];
                    dto = dto.getDto();
                    
                    $.post('/api/factors/' + dto.id, dto);
                }
            },
            addVector: function () {
            	var vectorname = prompt("Vector Name."),
            		vector = {
	            		name: vectorname,
	            		minimum: 0,
	            		maximum: 100
	            	};
            	$.post('/api/factors/Products/vectorTypes', {
            		vectorType: vector
            	});

            	expectedVectors.push(vector);

            	ko.utils.arrayForEach(products, function (product) {
            		product.addVector(vectorname);
            	});
            },
            addProduct: function () {
            	var productname = prompt("Product Name."),
            		product = {
            			name: productname,
            			description: 'test product'
            		};
            	$.post('/api/factors', {
            		factorType: 'Products',
            		factor: product
            	});

            	product.expectedVectors = expectedVectors();
            	product.vectors = [];
            	products.push(buildProduct(product));
            }
        };
    }
});
