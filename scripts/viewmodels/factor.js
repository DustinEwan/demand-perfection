
define(['knockout'],
	function (ko) {
    var factor = function (options) {
        var i, vector;
        
        options = options || {};
        this.id = ko.observable(options.id || 0);
        this.name = ko.observable(options.name || '');
        this.description = ko.observable(options.description || 0);
        this.vectors = {};
        
        options.vectors = options.vectors || [];
        for (i = options.vectors.length - 1; i >= 0; i--) {
            vector = options.vectors[i];
            this.addVector(vector.name, vector.value);
        }
        
        if (options.vectors.length < options.expectedVectors.length) {
            for (i = options.expectedVectors.length - 1; i >= 0; i--) {
                vector = options.expectedVectors[i];
                //this.addVector(vector.name, Math.floor(Math.random() * vector.maximum) + vector.minimum);
                this.addVector(vector.name, Math.random());
            }
        }
    },
        proto = factor.prototype;
    
    proto.addVector = function (name, value) {
        if (!this.vectors.hasOwnProperty(name)) {
            this.vectors[name] = ko.observable(value || 0);
        }
    }
    
    proto.updateVector = function (name, value) {
        this.vectors[name](value);
    }
    
    proto.getVector = function (name) {
        return this.vectors[name];
    }

    // in the future, add weights for this.
    proto.getVectorNormal = function () {
    	var total = 0;
    	for (var i in this.vectors) {
    		total = total + this.vectors[i]();
    	}

    	return total / Object.keys(this.vectors).length;
    }
    
    proto.getVectorValuesDto = function () {
        var vectorDto = [],
            vector = {},
            keys = Object.keys(this.vectors),
            name = '';

        for (var i = keys.length - 1; i >= 0; i--) {
            name = keys[i];
            vector = this.vectors[name];
            vectorDto.push({
                vector: { name: name },
                value: vector()
            });
        }

        return vectorDto;
    }
    
    proto.getDto = function () {
        return {
            id: this.id(),
            name: this.name(),
            description: this.description(),
            vectors: this.getVectorValuesDto()
        }
    }

    return factor;
});