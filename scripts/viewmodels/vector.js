
define(['knockout'],
	function(ko) {
		var vector = function (options) {
			options = options || {};
			this.id = ko.observable(options.id || 0);
			this.name = ko.observable(options.name || '');
			this.importance = ko.observable(options.name || 1);
			this.directRelationship = ko.observable(option.directRelationship || true);
			this.minimum = ko.observable(option.minimum || 0);
			this.maximum = ko.observable(option.maximum || 100);
		},
		proto = vector.prototype;

		proto.normalize = function (value) {
			var normal = (value - this.minimum) / this.maximum;

			return this.directRelationship
					? normal
					: 1 - normal;
		}

		return vector;
});
