
define(['knockout', 'viewmodels/factor'],
	function (ko, factor) {
    var buildCustomer = function (customerData) {
        return new factor(customerData);
    };
    
    return function (customerData) {
        var cd = customerData,
            customers = ko.observableArray([]),
            customer,
            expectedVectors = ko.observableArray(cd ? cd.vectors : []),
            customerData = cd ? cd.factors : [],
            data;
        

        for (var i in customerData) {
            data = cd.factors[i];
            data.expectedVectors = expectedVectors();            
            customer = buildCustomer(data);
            customers.push(customer);
        }

        return {
            customers: customers,
            vectors : expectedVectors,
            save: function () {
                for (var i = customers().length - 1; i >= 0; i--) {
                    var dto = customers()[i];
                    dto = dto.getDto();
                    
                    $.post('/api/factors/' + dto.id, dto);
                }
            },
            addVector: function () {
            	var vectorname = prompt("Vector Name."),
            		vector = {
	            		name: vectorname,
	            		minimum: 0,
	            		maximum: 100
	            	};
            	$.post('/api/factors/Customers/vectorTypes', {
            		vectorType: vector
            	});

            	expectedVectors.push(vector);

            	ko.utils.arrayForEach(customers, function (customer) {
            		customer.addVector(vectorname);
            	});
            },
            addCustomer: function () {
            	var customername = prompt("Customer Name."),
            		customer = {
            			name: customername,
            			description: 'test customer'
            		};
            	$.post('/api/factors', {
            		factorType: 'Customers',
            		factor: customer
            	});

            	customer.expectedVectors = expectedVectors();
            	customer.vectors = [];
            	customers.push(buildCustomer(customer));
            }
        };
    }
});
